var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const { check, validationResult } = require('express-validator/check');

/* GET users listing. */
router.get('/', function(req, res, next) {
  
  res.render('leaderboard');  
});
router.post('/', (req, res)=>{
  const error = {
    players: 'player\'s quantity incorrect',
    scores: 'scores can\'t be so much big',
    scoreslength: 'a lot of scores for those players',
    games: 'a lot of games',
    aliceScores: 'a lot of scores for alice',
    
  }
  players = req.body.players
  if(players< 1 || players > 200000){
    return res.status(200).json({errors: error.players });  
  }
  const scores = req.body.scores.split(" ")
  scores.forEach(score => {
    if(score <= 0 || score >1000000000){
      return res.status(200).json({ errors: error.scores });  
    }
  })
  console.log(scores.length);
  console.log(players);

  if(parseInt(scores.length) !== parseInt(players) ){
    return res.status(200).json({ errors: error.scoreslength });  
  }
  aliceGames = req.body.aliceGames
  if(parseInt(aliceGames) < 1 || parseInt(aliceGames) > 200000){
    return res.status(200).json({ errors: error.games });  
  }
  aliceScores = req.body.aliceScores.split(" ")
  if(parseInt(aliceScores.length) !== parseInt(aliceGames)){
    return res.status(200).json({ errors: error.aliceScores });  
  }
  aliceScores.forEach(score => {
    if(score < 1 || score > 200000){
      return res.status(200).json({  errors: error.games });  
    }
  })

  let aliceRanks = []
  aliceScores.forEach((element, i) => {
    const scoresWithAlice = req.body.scores.split(" ")
    element = {i:'alice', val:element}
    scoresWithAlice.push(element)
    sorted = scoresWithAlice.slice().sort(function(a,b){
      if(b.i){
        return b.val-a
      }else if(a.i){
        return b-a.val
      }else{
        return b-a
      }
    })
    ranks = scoresWithAlice.slice ().map(function(v){ return sorted.indexOf(v)+1 })
    aliceRanks[i] = ranks[scoresWithAlice.length-1];

  });
  return  res.render('leaderboard', { ranks: aliceRanks});  

})

module.exports = router;
